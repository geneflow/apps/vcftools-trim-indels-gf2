VCFTools Trim Indels App
========================

Version: 0.1.16-01

This GeneFlow2 app wraps the VCFTools function for trimming indels.

Inputs
------

1. input: Input VCF File

Parameters
----------

1. output: Output VCF File
